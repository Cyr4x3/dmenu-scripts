# dmenu scripts

My personal collection of [dmenu](https://tools.suckless.org/dmenu/) scripts for quick access to things I use on a daily basis.

These scripts are intented for my personal usage and some of them may not be finished and/or may not work properly (or not work at all). Use them under your own risk, as I won't take any responsability for any harm made to your system (although I don't see how some dmenu scripts may do that...).
I'll not provide any support to anyone using this scripts unless I feel like it, so don't expect me to solve your problems if you run into any of them while using these scripts.

## Credits

Some of this scripts are based on other people's scripts, so all credits to them.
I tried to add the name of the original author of the script in every one of them (except for the ones I wrote from scratch, obviously, where only my name is present).


## Contact

Feel free to contact me via email ( cyr4x3@disroot.org ) and follow me on my social media (and contact me through those sites). I'm not really active on there, but I may post something every now and then.
- [DeviantArt](https://www.deviantart.com/cyr4x3)
- [Mastodon](https://mastodon.online/web/accounts/125330)
- [Reddit](https://www.reddit.com/user/Cyr4x3)
